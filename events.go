package marvel

import (
	"fmt"
	"time"
)

type EventsService struct {
	client *Client
}

type EventDataWrapper struct {
	Code            int                 `json:"code,omitempty"`
	Status          string              `json:"status,omitempty"`
	Copyright       string              `json:"copyright,omitempty"`
	AttributionText string              `json:"attributionText,omitempty"`
	AttributionHTML string              `json:"attributionHTML,omitempty"`
	Data            *EventDataContainer `json:"data,omitempty"`
	Etag            string              `json:"etag,omitempty"`
}

type EventDataContainer struct {
	Offset  int      `json:"offset,omitempty"`
	Limit   int      `json:"limit,omitempty"`
	Total   int      `json:"total,omitempty"`
	Count   int      `json:"count,omitempty"`
	Results []*Event `json:"results,omitempty"`
}

type Event struct {
	ID          int            `json:"id,omitempty"`
	Title       string         `json:"title,omitempty"`
	Description string         `json:"description,omitempty"`
	ResourceURI string         `json:"resourceURI,omitempty"`
	Urls        []*Url         `json:"urls,omitempty"`
	Modified    string         `json:"modified,omitempty"` //TODO make this real time
	Start       string         `json:"start,omitempty"`    //TODO make this real time
	End         string         `json:"end,omitempty"`      //TODO make this real time
	Thumbnail   *Image         `json:"thumbnail,omitempty"`
	Comics      *ComicList     `json:"comics,omitempty"`
	Stories     *StoryList     `json:"stories,omitempty"`
	Series      *SeriesList    `json:"series,omitempty"`
	Characters  *CharacterList `json:"characters,omitempty"`
	Creators    *CreatorList   `json:"creators,omitempty"`
	Next        *SeriesSummary `json:"next,omitempty"`
	Previous    *SeriesSummary `json:"previous,omitempty"`
}

type GetEventsOptions struct {
	Name           *string    `url:"name,omitempty"`
	NameStartsWith *string    `url:"nameStartsWith,omitempty"`
	ModifiedSince  *time.Time `url:"modifiedSince,omitempty"`
	Creators       *int       `url:"creators,omitempty"`
	Characters     *int       `url:"characters,omitempty"`
	Series         *int       `url:"series,omitempty"`
	Comics         *int       `url:"comics,omitempty"`
	Stories        *int       `url:"stories,omitempty"`
	OrderBy        *string    `url:"orderBy,omitempty"`
	Limit          *int       `url:"limit,omitempty"`
	Offset         *int       `url:"offset,omitempty"`
}

func (s *EventsService) GetEvents(opt *GetEventsOptions) (*EventDataWrapper, *Response, error) {
	req, err := s.client.NewRequest("GET", "events", opt)
	if err != nil {
		return nil, nil, err
	}

	var c *EventDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}

func (s *EventsService) GetEvent(id int, opt *GetEventsOptions) (*EventDataWrapper, *Response, error) {
	u := fmt.Sprintf("events/%d", id)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *EventDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}
