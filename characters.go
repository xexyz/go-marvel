package marvel

import (
	"fmt"
	"time"
)

type CharactersService struct {
	client *Client
}

type CharacterDataWrapper struct {
	Code            int                     `json:"code,omitempty"`
	Status          string                  `json:"status,omitempty"`
	Copyright       string                  `json:"copyright,omitempty"`
	AttributionText string                  `json:"attributionText,omitempty"`
	AttributionHTML string                  `json:"attributionHTML,omitempty"`
	Data            *CharacterDataContainer `json:"data"`
	Etag            string                  `json:"etag,omitempty"`
}

type CharacterDataContainer struct {
	Offset  int          `json:"offset,omitempty"`
	Limit   int          `json:"limit,omitempty"`
	Total   int          `json:"total,omitempty"`
	Count   int          `json:"count,omitempty"`
	Results []*Character `json:"results"`
}

type Character struct {
	ID          int         `json:"id,omitempty"`
	Name        string      `json:"name,omitempty"`
	Description string      `json:"description,omitempty"`
	Modified    string      `json:"modified,omitempty"`
	ResourceURI string      `json:"resourceURI,omitempty"`
	Urls        []*Url      `json:"urls,omitempty"`
	Thumbnail   *Image      `json:"thumbnail,omitempty"`
	Comics      *ComicList  `json:"comics,omitempty"`
	Stories     *StoryList  `json:"stories,omitempty"`
	Events      *EventList  `json:"events,omitempty"`
	Series      *SeriesList `json:"series,omitempty"`
}

type Url struct {
	Type string `json:"type,omitempty"`
	Url  string `json:"url,omitempty"`
}

type Image struct {
	Path      string `json:"path,omitempty"`
	Extension string `json:"extension,omitempty"`
}

type ComicList struct {
	Available     int             `json:"available,omitempty"`
	Returned      int             `json:"returned,omitempty"`
	CollectionURI string          `json:"collectionURI,omitempty"`
	Items         []*ComicSummary `json:"items,omitempty"`
}

type ComicSummary struct {
	ResourceURI string `json:"resourceURI,omitempty"`
	Name        string `json:"name,omitempty"`
}

type StoryList struct {
	Available     int             `json:"available,omitempty"`
	Returned      int             `json:"returned,omitempty"`
	CollectionURI string          `json:"collectionURI,omitempty"`
	Items         []*StorySummary `json:"items,omitempty"`
}

type StorySummary struct {
	ResourceURI string `json:"resourceURI,omitempty"`
	Name        string `json:"name,omitempty"`
	Type        string `json:"type,omitempty"`
}

type EventList struct {
	Available     int             `json:"available,omitempty"`
	Returned      int             `json:"returned,omitempty"`
	CollectionURI string          `json:"collectionURI,omitempty"`
	Items         []*EventSummary `json:"items,omitempty"`
}

type EventSummary struct {
	ResourceURI string `json:"resourceURI,omitempty"`
	Name        string `json:"name,omitempty"`
}

type SeriesList struct {
	Available     int              `json:"available,omitempty"`
	Returned      int              `json:"returned,omitempty"`
	CollectionURI string           `json:"collectionURI,omitempty"`
	Items         []*SeriesSummary `json:"items,omitempty"`
}

type SeriesSummary struct {
	ResourceURI string `json:"resourceURI,omitempty"`
	Name        string `json:"name,omitempty"`
}

// ListCharactersOptions represents the available Characters() options.
type GetCharactersOptions struct {
	Name           *string    `url:"name,omitempty"`
	NameStartsWith *string    `url:"nameStartsWith,omitempty"`
	ModifiedSince  *time.Time `url:"modifiedSince,omitempty"`
	Comics         *int       `url:"comics,omitempty"`
	Series         *int       `url:"series,omitempty"`
	Events         *int       `url:"events,omitempty"`
	Stories        *int       `url:"stores,omitempty"`
	OrderBy        *string    `url:"orderBy,omitempty"`
	Limit          *int       `url:"limit,omitempty"`
	Offset         *int       `url:"offset,omitempty"`
}

func (s *CharactersService) GetCharacters(opt *GetCharactersOptions) (*CharacterDataWrapper, *Response, error) {
	req, err := s.client.NewRequest("GET", "characters", opt)
	if err != nil {
		return nil, nil, err
	}

	var c *CharacterDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}

// GetCharacters gets a list of characters
func (s *CharactersService) GetCharacter(id int, opt *GetCharactersOptions) (*CharacterDataWrapper, *Response, error) {
	u := fmt.Sprintf("characters/%d", id)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *CharacterDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}
