package marvel

import (
	"fmt"
	"time"
)

// CharactersService handles communcation with the character related methods
// of the Marvel API
type ComicsService struct {
	client *Client
}

// Character represents a character

type ComicDataWrapper struct {
	Code            int                 `json:"code,omitempty"`
	Status          string              `json:"status,omitempty"`
	Copyright       string              `json:"copyright,omitempty"`
	AttributionText string              `json:"attributionText,omitempty"`
	AttributionHTML string              `json:"attributionHTML,omitempty"`
	Data            *ComicDataContainer `json:"data,omitempty"`
	Etag            string              `json:"etag,omitempty"`
}

type ComicDataContainer struct {
	Offset  int      `json:"offset,omitempty"`
	Limit   int      `json:"limit,omitempty"`
	Total   int      `json:"total,omitempty"`
	Count   int      `json:"count,omitempty"`
	Results []*Comic `json:"results,omitempty"`
}

type Comic struct {
	ID                 int             `json:"id,omitempty"`
	DigitalId          int             `json:"digitalId,omitempty"`
	Title              string          `json:"title,omitempty"`
	IssueNumber        float64         `json:"issueNumber,omitempty"`
	VariantDescription string          `json:"variantDescription,omitempty"`
	Description        string          `json:"description,omitempty"`
	Modified           string          `json:"modified,omitempty"`
	ISBN               string          `json:"isbn,omitempty"`
	UPC                string          `json:"upc,omitempty"`
	DiamondCode        string          `json:"diamondCode,omitempty"`
	EAN                string          `json:"ean,omitempty"`
	ISSN               string          `json:"issn,omitempty"`
	Format             string          `json:"format,omitempty"`
	PageCount          int             `json:"pageCount,omitempty"`
	TextObjects        []*TextObject   `json:"textObjects,omitempty"`
	ResourceURI        string          `json:"resourceURI,omitempty"`
	Urls               []*Url          `json:"urls,omitempty"`
	Series             *SeriesSummary  `json:"series,omitempty"`
	Variants           []*ComicSummary `json:"variants,omitempty"`
	Collections        []*ComicSummary `json:"collections,omitempty"`
	CollectedIssues    []*ComicSummary `json:"collectedIssues,omitempty"`
	Dates              []*ComicDate    `json:"dates,omitempty"`
	Prices             []*ComicPrice   `json:"prices,omitempty"`
	Thumbnail          *Image          `json:"thumbnail,omitempty"`
	Images             []*Image        `json:"images,omitempty"`
	Creators           *CreatorList    `json:"creators,omitempty"`
	Characters         *CharacterList  `json:"CharacterList,omitempty"`
	Stories            *StoryList      `json:"stories,omitempty"`
	Events             *EventList      `json:"events,omitempty"`
}

type TextObject struct {
	Type     string `json:"type,omitempty"`
	Language string `json:"language,omitempty"`
	Text     string `json:"text,omitempty"`
}

// type Url struct {
// 	Type                                          string                      `json:"type,omitempty"`
// 	Url                                           string                      `json:"url,omitempty"`
// }

// type SeriesSummary struct {
// 	ResourceURI                                   string                      `json:"resourceURI,omitempty"`
// 	Name                                          string                      `json:"name,omitempty"`
// }

// type ComicSummary struct {
// 	ResourceURI                                   string                      `json:"resourceURI,omitempty"`
// 	Name                                          string                      `json:"name,omitempty"`
// }

type ComicDate struct {
	Type string `json:"type,omitempty"`
	Date string `json:"date,omitempty"` //TODO make this real time
}

type ComicPrice struct {
	Type  string  `json:"type,omitempty"`
	Price float32 `json:"price,omitempty"`
}

// type Image struct {
// 	Path                                          string                      `json:"path,omitempty"`
// 	Extension                                     string                      `json:"extension,omitempty"`
// }

type CreatorList struct {
	Available     int               `json:"available,omitempty"`
	Returned      int               `json:"returned,omitempty"`
	CollectionURI string            `json:"collectionURI,omitempty"`
	Items         []*CreatorSummary `json:"items,omitempty"`
}

type CreatorSummary struct {
	ResourceURI string `json:"resourceURI,omitempty"`
	Name        string `json:"name,omitempty"`
	Role        string `json:"role,omitempty"`
}

type CharacterList struct {
	Available     int                 `json:"available,omitempty"`
	Returned      int                 `json:"returned,omitempty"`
	CollectionURI string              `json:"collectionURI,omitempty"`
	Items         []*CharacterSummary `json:"items,omitempty"`
}

type CharacterSummary struct {
	ResourceURI string `json:"resourceURI,omitempty"`
	Name        string `json:"name,omitempty"`
	Role        string `json:"role,omitempty"`
}

// type StoryList struct {
// 	Available                                     int                         `json:"available,omitempty"`
// 	Returned                                      int                         `json:"returned,omitempty"`
// 	CollectionURI                                 string                      `json:"collectionURI,omitempty"`
// 	Items                                         []*StorySummary             `json:"items,omitempty"`
// }

// type StorySummary struct {
// 	ResourceURI                                   string                      `json:"resourceURI,omitempty"`
// 	Name                                          string                      `json:"name,omitempty"`
// 	Type                                          string                      `json:"type,omitempty"`
// }

// type EventList struct {
// 	Available                                     int                         `json:"available,omitempty"`
// 	Returned                                      int                         `json:"returned,omitempty"`
// 	CollectionURI                                 string                      `json:"collectionURI,omitempty"`
// 	Items                                         []*EventSummary             `json:"items,omitempty"`
// }

// type EventSummary struct {
// 	ResourceURI                                   string                      `json:"resourceURI,omitempty"`
// 	Name                                          string                      `json:"name,omitempty"`
// }

// ListCharactersOptions represents the available Characters() options.
type GetComicsOptions struct {
	Format            *string    `url:"format,omitempty"`
	FormatType        *string    `url:"formatType,omitempty"`
	NoVariants        *bool      `url:"noVariants,omitempty"`
	DateDescriptor    *string    `url:"dateDescriptor,omitempty"`
	DateRange         *int       `url:"dateRange,omitempty"`
	Title             *string    `url:"title,omitempty"`
	TitleStartsWith   *string    `url:"titleStartsWith,omitempty"`
	StartYear         *int       `url:"startYear,omitempty"`
	IssueNumber       *int       `url:"issueNumber,omitempty"`
	DiamondCode       *string    `url:"diamondCode,omitempty"`
	DigitalID         *int       `url:"digitalId,omitempty"`
	UPC               *string    `url:"upc,omitempty"`
	ISBN              *string    `url:"isbn,omitempty"`
	EAN               *string    `url:"ean,omitempty"`
	ISSN              *string    `url:"issn,omitempty"`
	HasDigitalIssue   *bool      `url:"hasDigitalIssue,omitempty"`
	ModifiedSince     *time.Time `url:"modifiedSince,omitempty"`
	Creators          *int       `url:"creators,omitempty"`
	Series            *int       `url:"series,omitempty"`
	Events            *int       `url:"events,omitempty"`
	Stories           *int       `url:"stories,omitempty"`
	SharedAppearances *int       `url:"sharedAppearances,omitempty"`
	Collaborators     *int       `url:"collaboarators,omitempty"`
	OrderBy           *string    `url:"orderBy,omitempty"`
	Limit             *int       `url:"limit,omitempty"`
	Offset            *int       `url:"offset,omitempty"`
}

// GetCharacters gets a list of characters
func (s *ComicsService) GetComics(opt *GetComicsOptions) (*ComicDataWrapper, *Response, error) {
	req, err := s.client.NewRequest("GET", "comics", opt)
	if err != nil {
		return nil, nil, err
	}

	var c *ComicDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}

func (s *ComicsService) GetComic(id int, opt *GetComicsOptions) (*ComicDataWrapper, *Response, error) {
	u := fmt.Sprintf("comics/%d", id)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *ComicDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}
