package marvel

import (
	"fmt"
	"net/http"
	"reflect"
	"testing"
)

func TestGetCharacters(t *testing.T) {
	mux, server, client := setup()
	defer teardown(server)

	mux.HandleFunc("/characters", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		fmt.Fprint(w, `[{"id":1},{"id":2}]`)
	})
	want := &CharacterDataWrapper{Code: 1}
	characters, _, err := client.Characters.GetCharacters(nil)

	if err != nil {
		t.Errorf("Characters.GetCharacters returned error: %v", err)
	}

	if !reflect.DeepEqual(want, characters) {
		t.Errorf("Characters.GetCharacters returned %v, want %v", characters, want)
	}

}
