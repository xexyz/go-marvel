package marvel

import (
	"fmt"
	"time"
)

// CharactersService handles communcation with the character related methods
// of the Marvel API
type CreatorsService struct {
	client *Client
}

// Character represents a character

type CreatorDataWrapper struct {
	Code            int                   `json:"code,omitempty"`
	Status          string                `json:"status,omitempty"`
	Copyright       string                `json:"copyright,omitempty"`
	AttributionText string                `json:"attributionText,omitempty"`
	AttributionHTML string                `json:"attributionHTML,omitempty"`
	Data            *CreatorDataContainer `json:"data,omitempty"`
	Etag            string                `json:"etag,omitempty"`
}

type CreatorDataContainer struct {
	Offset  int        `json:"offset,omitempty"`
	Limit   int        `json:"limit,omitempty"`
	Total   int        `json:"total,omitempty"`
	Count   int        `json:"count,omitempty"`
	Results []*Creator `json:"results,omitempty"`
}

type Creator struct {
	ID          int            `json:"id,omitempty"`
	FirstName   string         `json:"firstName,omitempty"`
	MiddleName  string         `json:"middleName,omitempty"`
	LastName    string         `json:"lastName,omitempty"`
	Suffix      string         `json:"suffix,omitempty"`
	FullName    string         `json:"fullName,omitempty"`
	Modified    string         `json:"modified,omitempty"` //TODO make this real time
	ResourceURI string         `json:"resourceURI,omitempty"`
	Urls        []*Url         `json:"urls,omitempty"`
	Thumbnail   *Image         `json:"thumbnail,omitempty"`
	Series      *SeriesList    `json:"series,omitempty"`
	Stories     *StoryList     `json:"stories,omitempty"`
	Comics      *ComicList     `json:"comics,omitempty"`
	Characters  *CharacterList `json:"characters,omitempty"`
	Events      *EventList     `json:"events,omitempty"`
}

// ListCharactersOptions represents the available Characters() options.
type GetCreatorsOptions struct {
	FirstName            *string    `url:"firstName,omitempty"`
	MiddleName           *string    `url:"middleName,omitempty"`
	LastName             *string    `url:"lastName,omitempty"`
	Suffix               *string    `url:"suffix,omitempty"`
	NameStartsWith       *string    `url:"nameStartsWith,omitempty"`
	FirstNameStartsWith  *string    `url:"firstNameStartsWith,omitempty"`
	MiddleNameStartsWith *string    `url:"middleNameStartsWith,omitempty"`
	LastNameStartsWith   *string    `url:"lastNameStartsWith,omitempty"`
	ModifiedSince        *time.Time `url:"modifiedSince,omitempty"`
	Comics               *int       `url:"comics,omitempty"`
	Series               *int       `url:"series,omitempty"`
	Events               *int       `url:"events,omitempty"`
	Stories              *int       `url:"stories,omitempty"`
	OrderBy              *string    `url:"orderBy,omitempty"`
	Limit                *int       `url:"limit,omitempty"`
	Offset               *int       `url:"offset,omitempty"`
}

// GetCharacters gets a list of characters
func (s *CreatorsService) GetCreators(opt *GetCreatorsOptions) (*CreatorDataWrapper, *Response, error) {
	req, err := s.client.NewRequest("GET", "creators", opt)
	if err != nil {
		return nil, nil, err
	}

	var c *CreatorDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}

func (s *CreatorsService) GetCreator(id int, opt *GetCreatorsOptions) (*CreatorDataWrapper, *Response, error) {
	u := fmt.Sprintf("creators/%d", id)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *CreatorDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}
