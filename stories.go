// Fetches lists of comic stories with optional filters
package marvel

import (
	"fmt"
	"time"
)

type StoriesService service

type StoryDataWrapper struct {
	Code            int                 `json:"code,omitempty"`
	Status          string              `json:"status,omitempty"`
	Copyright       string              `json:"copyright,omitempty"`
	AttributionText string              `json:"attributionText,omitempty"`
	AttributionHTML string              `json:"attributionHTML,omitempty"`
	Data            *StoryDataContainer `json:"data,omitempty"`
	Etag            string              `json:"etag,omitempty"`
}

type StoryDataContainer struct {
	Offset  int      `json:"offset,omitempty"`
	Limit   int      `json:"limit,omitempty"`
	Total   int      `json:"total,omitempty"`
	Count   int      `json:"count,omitempty"`
	Results []*Story `json:"results,omitempty"`
}

type Story struct {
	ID            int            `json:"id,omitempty"`
	Title         string         `json:"title,omitempty"`
	Description   string         `json:"description,omitempty"`
	ResourceURI   string         `json:"resourceURI,omitempty"`
	Type          string         `json:"type,omitempty"`
	Modified      string         `json:"modified,omitempty"` //TODO make this real time
	Thumbnail     *Image         `json:"thumbnail,omitempty"`
	Comics        *ComicList     `json:"comics,omitempty"`
	Series        *SeriesList    `json:"series,omitempty"`
	Events        *EventList     `json:"events,omitempty"`
	Characters    *CharacterList `json:"characters,omitempty"`
	Creators      *CreatorList   `json:"creators,omitempty"`
	OriginalIssue *ComicSummary  `json:"originalIssue,omitempty"`
}

// ListCharactersOptions represents the available Characters() options.
type GetStoriesOptions struct {
	ModifiedSince *time.Time `url:"modifiedSince,omitempty"`
	Comics        *int       `url:"comics,omitempty"`
	Series        *int       `url:"series,omitempty"`
	Events        *int       `url:"events,omitempty"`
	Creators      *int       `url:"creators,omitempty"`
	Characters    *int       `url:"characters,omitempty"`
	OrderBy       *string    `url:"orderBy,omitempty"`
	Limit         *int       `url:"limit,omitempty"`
	Offset        *int       `url:"offset,omitempty"`
}

// GetCharacters gets a list of characters
func (s *StoriesService) GetStories(opt *GetStoriesOptions) (*StoryDataWrapper, *Response, error) {
	req, err := s.client.NewRequest("GET", "stories", opt)
	if err != nil {
		return nil, nil, err
	}

	var c *StoryDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}

func (s *StoriesService) GetStory(id int, opt *GetStoriesOptions) (*StoryDataWrapper, *Response, error) {
	u := fmt.Sprintf("stories/%d", id)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *StoryDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}
