// Fetches lists of comic series with optional filters.
package marvel

import (
	"fmt"
	"time"
)

type SeriesService service

type SeriesDataWrapper struct {
	Code            int                  `json:"code,omitempty"`
	Status          string               `json:"status,omitempty"`
	Copyright       string               `json:"copyright,omitempty"`
	AttributionText string               `json:"attributionText,omitempty"`
	AttributionHTML string               `json:"attributionHTML,omitempty"`
	Data            *SeriesDataContainer `json:"data,omitempty"`
	Etag            string               `json:"etag,omitempty"`
}

type SeriesDataContainer struct {
	Offset  int       `json:"offset,omitempty"`
	Limit   int       `json:"limit,omitempty"`
	Total   int       `json:"total,omitempty"`
	Count   int       `json:"count,omitempty"`
	Results []*Series `json:"results,omitempty"`
}

type Series struct {
	ID          int            `json:"id,omitempty"`
	Title       string         `json:"title,omitempty"`
	Description string         `json:"description,omitempty"`
	ResourceURI string         `json:"resourceURI,omitempty"`
	Urls        []*Url         `json:"urls,omitempty"`
	StartYear   int            `json:"startYear,omitempty"`
	EndYear     int            `json:"endYear,omitempty"`
	Rating      string         `json:"rating,omitempty"`
	Modified    string         `json:"modified,omitempty"` //TODO make this real time
	Thumbnail   *Image         `json:"thumbnail,omitempty"`
	Comics      *ComicList     `json:"comics,omitempty"`
	Stories     *StoryList     `json:"stories,omitempty"`
	Events      *EventList     `json:"events,omitempty"`
	Characters  *CharacterList `json:"characters,omitempty"`
	Creators    *CreatorList   `json:"creators,omitempty"`
	Next        *SeriesSummary `json:"next,omitempty"`
	Previous    *SeriesSummary `json:"previous,omitempty"`
}

type GetSeriesOptions struct {
	Title           *string    `url:"title,omitempty"`
	TitleStartsWith *string    `url:"titleStartsWith,omitempty"`
	StartYear       *int       `url:"startYear,omitempty"`
	ModifiedSince   *time.Time `url:"modifiedSince,omitempty"`
	Comics          *int       `url:"comics,omitempty"`
	Stories         *int       `url:"stories,omitempty"`
	Events          *int       `url:"events,omitempty"`
	Creators        *int       `url:"creators,omitempty"`
	Characters      *int       `url:"characters,omitempty"`
	SeriesType      *string    `url:"seriesType,omitempty"`
	Contains        *string    `url:"contains,omitempty"`
	OrderBy         *string    `url:"orderBy,omitempty"`
	Limit           *int       `url:"limit,omitempty"`
	Offset          *int       `url:"offset,omitempty"`
}

func (s *SeriesService) GetSeries(opt *GetSeriesOptions) (*SeriesDataWrapper, *Response, error) {
	req, err := s.client.NewRequest("GET", "series", opt)
	if err != nil {
		return nil, nil, err
	}

	var c *SeriesDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}

func (s *SeriesService) GetASeries(id int, opt *GetSeriesOptions) (*SeriesDataWrapper, *Response, error) {
	u := fmt.Sprintf("series/%d", id)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *SeriesDataWrapper
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}
